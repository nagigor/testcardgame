﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : MonoBehaviour
{
    public GameObject myHand;
    public GameObject deck;

    public List<UIGrid> slots = new List<UIGrid>();

    public List<CardBase> cm = new List<CardBase>();

	void Start ()
    {
        ListofCards.InitCards();
        cm = ListofCards.cards;

        for (int i = 0; i < 3; i++) // рука игрока
        {
            GameObject preObj = Resources.Load("Prefabs/Card") as GameObject;
            GameObject prefab = myHand.AddChild(preObj);
            int randomCard = Random.Range(0, 5);
            prefab.GetComponent<CardPrefabScript>().index = randomCard;
            prefab.GetComponent<BoxCollider>().enabled = true;
            myHand.GetComponent<UIGrid>().repositionNow = true;
        }

        for (int i = 0; i < 5; i++) // колода игрока
        {
            GameObject preObj = Resources.Load("Prefabs/Card") as GameObject;
            GameObject prefab = deck.AddChild(preObj);
            prefab.GetComponent<UIPanel>().depth = i + 1;
            int randomCard = Random.Range(0, 5);
            prefab.GetComponent<CardPrefabScript>().index = randomCard;
            deck.GetComponent<UIGrid>().repositionNow = true;
            
        }

    }
	
	void Update ()
    {
        CardsReparent();
    }

    void CardsReparent()
    {
        for (int i = 0; i < slots.Count; i++)
        {
            if (slots[i].GetChildList().Count > 0)
            {
                slots[i].GetComponent<UIDragDropContainer>().reparentTarget = myHand.transform;
                slots[i].GetChild(0).GetComponent<BoxCollider>().enabled = false;
            }
            else
            {
                slots[i].GetComponent<UIDragDropContainer>().reparentTarget = slots[i].transform;
            }
        }
    }

    public void ReturnCards()
    {
        for (int i = 0; i < slots.Count; i++)
        {
            if(slots[i].GetChildList().Count > 0) 
                slots[i].GetChild(0).SetParent(deck.transform);
        }
        
        for (int i = 0; i < deck.GetComponent<UIGrid>().GetChildList().Count; i++)
        {
            deck.GetComponent<UIGrid>().GetChild(i).GetComponent<UIPanel>().depth = deck.GetComponent<UIGrid>().GetChildList().Count-i;
            deck.GetComponent<UIGrid>().GetChild(i).GetComponent<CardPrefabScript>().TweenScaleReverse();
            deck.GetComponent<UIGrid>().GetChild(i).GetComponent<CardPrefabScript>().currentHealth = deck.GetComponent<UIGrid>().GetChild(i).GetComponent<CardPrefabScript>().maxHealth;
            deck.GetComponent<UIGrid>().GetChild(i).GetComponent<CardPrefabScript>().RefreshCardStatus();
            deck.GetComponent<UIGrid>().repositionNow = true;
        }
        


    }

    public void GetCard()
    {
        if(myHand.GetComponent<UIGrid>().GetChildList().Count < 3)
        {
            deck.GetComponent<UIGrid>().GetChild(0).GetComponent<BoxCollider>().enabled = true;
            deck.GetComponent<UIGrid>().GetChild(0).SetParent(myHand.transform);
            myHand.GetComponent<UIGrid>().repositionNow = true;
        }
    }

    public void MakeDamage()
    {
        for (int i = 0; i < slots.Count; i++)
        {
            if (slots[i].GetChildList().Count > 0)
                slots[i].GetChild(0).GetComponent<CardPrefabScript>().ApplyDamage();
        }
    }
    
}
