﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class CardBase
{
    public int damage;
    public int health;
    public string title;
    public string description;
    public string image;

    protected iAbilities skillBehaviour;
    
    
    public CardBase(int damage, int health, string title, string description, string image)
    {
        
        this.damage = damage;
        this.health = health;
        this.title = title;
        this.description = description;
        this.image = image;
        skillBehaviour = new Abilities();
    }
    public void Skill()
    {
        skillBehaviour.Ability();
    }
}

public static class ListofCards
{    
    public static List<CardBase> cards = new List<CardBase>();

    public static void InitCards()
    {
        cards.Add(new Orc(damage: 1, health: 2, title: "Wolf", description: "Рывок", image: "WolfCard"));
        cards.Add(new Wolf(damage : 3, health : 4, title : "Orc", description : "Берсерк", image : "OrcCard"));
        cards.Add(new Orc(damage: 1, health: 2, title: "Wolf", description: "Рывок", image: "WolfCard"));
        cards.Add(new Wolf(damage: 3, health: 4, title: "Orc", description: "Берсерк", image: "OrcCard"));
        cards.Add(new Orc(damage: 1, health: 2, title: "Wolf", description: "Рывок", image: "WolfCard"));
    }
}
public class Orc : CardBase
{

    public Orc(int damage, int health,string title, string description , string image) : base( damage,  health,  title,  description, image)
    {
        skillBehaviour = new Charge();        
    }
}
public class Wolf : CardBase
{

    public Wolf(int damage, int health, string title, string description, string image) : base(damage, health, title, description, image)
    {
        
    }
}
