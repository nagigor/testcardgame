﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardPrefabScript : MonoBehaviour
{
    public UILabel damage;
    public UILabel health;
    public UILabel title;
    public UILabel description;
    public UISprite image;
    public TweenScale cardTS;
    public int currentHealth;
    public int maxHealth;

    public int index;

    void Start ()
    {
        currentHealth = ListofCards.cards[index].health;
        maxHealth = ListofCards.cards[index].health;
        RefreshCardStatus();

	}

    public void RefreshCardStatus()
    {
        damage.text = ListofCards.cards[index].damage.ToString();
        health.text = currentHealth.ToString();
        title.text = ListofCards.cards[index].title;
        description.text = ListofCards.cards[index].description;
        image.spriteName = ListofCards.cards[index].image;
    }   
    
    public void ApplyDamage()
    {
        if (currentHealth > 0)
        {
            currentHealth--;
            ListofCards.cards[index].Skill();
        }
        RefreshCardStatus();
    } 

    public void TweenScaleStart()
    {
        cardTS.from = new Vector2(2f, 2f);
        cardTS.to = new Vector2(1f, 1f);
        cardTS.PlayForward();
    }
    public void TweenScaleReverse()
    {
        cardTS.to = new Vector2(0.5f, 0.5f);
        cardTS.from = new Vector2(1f, 1f);
        cardTS.PlayReverse();
    }
    


}
